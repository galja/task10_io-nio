package com.epam.clientserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {

    private static final int BUFFER_SIZE = 1024;
    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        InetAddress address = InetAddress.getLocalHost();
        int port = 8888;

        InetSocketAddress inetSocketAddress = new InetSocketAddress(address, port);

        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(inetSocketAddress);

        serverSocketChannel.configureBlocking(false);
       // int ops = serverSocketChannel.validOps();
        //SelectionKey selectionKey = serverSocketChannel.register(selector, ops, null);
        System.out.println("server");

        while (true) {
            selector.select();
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> selectionKeyIterator = selectionKeys.iterator();

            while (selectionKeyIterator.hasNext()) {
                SelectionKey key = selectionKeyIterator.next();

                if (key.isAcceptable()) {
                    SocketChannel client = serverSocketChannel.accept();
                    client.configureBlocking(false);

                    client.register(selector, SelectionKey.OP_READ);
                    System.out.println("connection:" + client.getLocalAddress());

                } else if (key.isReadable()) {
                    SocketChannel client = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                    client.read(buffer);
                    String receivedMessage = new String(buffer.array()).trim();
                    if(receivedMessage.length()>0){
                        System.out.println("received message: "+receivedMessage);
                        if(receivedMessage.equalsIgnoreCase("*exit*")){
                            client.close();
                            System.out.println("closed");
                        }
                    }
                }
                selectionKeyIterator.remove();
            }
        }
    }
}