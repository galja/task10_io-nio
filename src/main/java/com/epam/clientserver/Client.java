package com.epam.clientserver;


import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
    private static final int BUFFER_SIZE = 1024;
    private static String[] messages = {"lkse", "kje"};

    public static void main(String[] args) {
        System.out.println("client");

        try {
        int port = 8888;

            InetAddress address = InetAddress.getLocalHost();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(address, port);
            SocketChannel client = SocketChannel.open(inetSocketAddress);

            System.out.println("try to connect to" + inetSocketAddress.getHostName() + ":" + inetSocketAddress.getPort());

            for (String message : messages) {
                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                buffer.put(message.getBytes());
                buffer.flip();
                int bytes = client.write(buffer);

                System.out.println("sending: message "+message+"bytes "+bytes);
            }
                client.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}