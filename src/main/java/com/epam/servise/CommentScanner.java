package com.epam.servise;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

public class CommentScanner {

    private static final String COMMENT = "//";

    public String getCommentsFromFile(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            StringBuilder data = new StringBuilder();
            String str = reader.readLine();
            while (Objects.nonNull(str)) {
                if (str.contains(COMMENT)) {
                    data.append(str.substring(str.indexOf(COMMENT))).append("\n");
                }
                str = reader.readLine();
            }
            return data.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}
