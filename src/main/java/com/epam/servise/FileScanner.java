package com.epam.servise;

import com.epam.helper.Helper;
import com.epam.models.CustomScanner;

import java.io.*;
import java.util.Objects;
import java.util.Properties;

public class FileScanner {
    private Properties properties;
    private static final String propertiesFileName = "config.properties";
    private static final String STRING = "lkwje fiej ";
    private static final String COMMENT = "//";

    public FileScanner() {
        properties = new Properties();
        try {
            properties.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(propertiesFileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readWithCustomScanner() {
        StringBuilder result = new StringBuilder();
        result.append(Helper.CUSTOM_SCANNER_READ);
        try (CustomScanner push = new CustomScanner(new ByteArrayInputStream(STRING.getBytes()))) {
            int by = push.read();
            for (int i = 0; i < 4; i++) {
                by = push.read();
                result.append(by);
            }
            push.unread(by);
            result.append(Helper.CUSTOM_SCANNER_AFTER_UNREAD);
            while ((by = push.read()) != -1) {
                result.append(by);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public long readFileWithFileInputSteam() {
        long executionTime = 0;
        try (InputStream inputStream = new DataInputStream(new FileInputStream(properties.getProperty("file1")));
             OutputStream outputStream = new DataOutputStream(new FileOutputStream(properties.getProperty("outputFile")))) {
            executionTime = calculateExecutionTime(inputStream, outputStream);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return executionTime;
    }

    public long readFileWithBufferedInputStream(int bufferSize) {
        long executionTime = 0;
        try (InputStream inputStream = new DataInputStream(new BufferedInputStream(
                new FileInputStream(properties.getProperty("file1")), bufferSize));
             OutputStream outputStream = new DataOutputStream(new BufferedOutputStream(
                     new FileOutputStream(properties.getProperty("outputFile")), bufferSize))) {
            executionTime = calculateExecutionTime(inputStream, outputStream);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return executionTime;
    }

    private long calculateExecutionTime(InputStream inputStream, OutputStream outputStream) throws IOException {
        long startTime = System.currentTimeMillis();
        int data = inputStream.read();
        while (data != -1) {
            outputStream.write(data);
            data = inputStream.read();
        }
        long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }

    public static void main(String[] args) {

//        new FileScanner().getCommentsFromFile();

    }
}
