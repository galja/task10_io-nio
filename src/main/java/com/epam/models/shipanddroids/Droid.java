package com.epam.models.shipanddroids;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int health;
    private transient int creationDate;

    public Droid(String name, int health, int creationDate) {
        this.name = name;
        this.health = health;
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(int creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", creationDate=" + creationDate +
                '}';
    }
}
