package com.epam.models.shipanddroids;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ship implements Serializable {
    private String name;
    private List<Droid> droids;
    private transient int tonnage;

    public Ship(){
        droids = new ArrayList<>();
    }

    public Ship(String name, int tonnage, List<Droid> droids) {
        this.droids = new ArrayList<>();
        this.name = name;
        this.droids = droids;
        this.tonnage = tonnage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Droid> getDroids() {
        return droids;
    }

    public void setDroids(List<Droid> droids) {
        this.droids = droids;
    }

    public void add(Droid... droids){
        this.droids = Arrays.asList(droids);
    }

    public int getTonnage() {
        return tonnage;
    }

    public void setTonnage(int tonnage) {
        this.tonnage = tonnage;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", droids=" + droids +
                ", tonnage=" + tonnage +
                '}';
    }
}
