package com.epam.models;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomScanner extends FilterInputStream {
    private byte[] buffer;
    private int position;

    public CustomScanner(InputStream in) {
        super(in);
        this.buffer = new byte[1];
        this.position = 1;
    }

    public int read() throws IOException {
        if (position < buffer.length) {
            return buffer[position++];
        }
        return super.read();
    }

    public void unread(int byteToUndread) throws IOException {
        if(position!=0) {
            buffer[--position] = (byte)byteToUndread;
        }else{
            throw new IOException();
        }
    }
}
