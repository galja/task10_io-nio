package com.epam.models;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class FileManager {

    private static final String DRIVE = "C:/";
    private static final String CD = "cd ";
    private static final String DIR = "dir";
    private static final String SLASH = "/";
    private static final String WRONG_COMMAND = "Wrong command";
    private String currentDirectory;
    private File file;

    public FileManager() {
        currentDirectory = DRIVE;
        file = new File(currentDirectory);
    }

    public String process(String command) {
        if (command.contains(CD)) {
            command = command.split(CD)[1];
            if (!command.contains(DRIVE)) {
                command = currentDirectory + command;
            }
            return changeDirectory(command);
        } else if (command.contains(DIR)) {
            return getFilesFromDirectory();
        } else {
            return WRONG_COMMAND;
        }
    }

    private String changeDirectory(String path) {
        if (path.endsWith(SLASH)) {
            currentDirectory = path;
        } else {
            currentDirectory = path + SLASH;
        }
        file = new File(currentDirectory);
        return currentDirectory;
    }

    private String getFilesFromDirectory() {
        return currentDirectory+"\n"+Arrays.stream(Objects.requireNonNull(file.listFiles()))
                .map(File::toString).collect(Collectors.joining("\n\t"));
    }
}
