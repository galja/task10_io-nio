package com.epam.models;

import com.epam.helper.Helper;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.Properties;

public class SomeBuffer {

    private ByteBuffer buffer;
    private Properties properties;

    public SomeBuffer(){
        properties = new Properties();
        try {
            properties.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(Helper.PROPERTIES_FILE_NAME)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readBytes(File file, int bufferCapacity) {
        StringBuilder stringBuilder = new StringBuilder();
        try (FileChannel channel = FileChannel.open(file.toPath(), StandardOpenOption.READ)) {
             buffer = ByteBuffer.allocate(bufferCapacity);
            int bytesRead = channel.read(buffer);
            if (bytesRead != -1) {
                buffer.rewind();
                for (int i = 0; i < bytesRead; i++) {
                    stringBuilder.append((char) buffer.get());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public void writeBytes(Path path, String data) {
        try(FileChannel channel = FileChannel.open(path, StandardOpenOption.CREATE)) {
             buffer = ByteBuffer.wrap(data.getBytes());
            channel.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        SomeBuffer someBuffer = new SomeBuffer();
////        someBuffer.writer();
//        someBuffer.readBytes(Paths.get("out.txt"), 4);
//        System.out.println();
//        someBuffer.readBytes(Paths.get("out.txt"), 20);
//
//    }
}


