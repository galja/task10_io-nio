package com.epam.controller;

import com.epam.helper.Helper;
import com.epam.models.FileManager;
import com.epam.models.SomeBuffer;
import com.epam.models.shipanddroids.Droid;
import com.epam.models.shipanddroids.Ship;
import com.epam.servise.CommentScanner;
import com.epam.servise.FileScanner;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class Controller {
    private FileManager fileManager;
    private static Ship ship;
    private static Properties properties;
    private SomeBuffer someBuffer;
    File file;

    public Controller() {
        someBuffer = new SomeBuffer();
        file = new File(properties.getProperty("fileForSomeBuffer"));
        fileManager = new FileManager();
        List<Droid> droidList = new ArrayList<>();
        droidList.add(Helper.FIRST_DROID);
        droidList.add(Helper.SECOND_DROID);
        ship = new Ship(Helper.SHIP_NAME, Helper.SHIP_TONNAGE, droidList);
        properties = new Properties();
        try {
            properties.load(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(Helper.PROPERTIES_FILE_NAME)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String testSerialization() throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(properties.getProperty("file2")));
        outputStream.writeObject(ship);
        outputStream.close();
        return ship.toString();
    }

    public String testDeserialization() throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(properties.getProperty("file2")));
        Ship deserializedShip = (Ship) inputStream.readObject();
        inputStream.close();
        return deserializedShip.toString();
    }

    public String compareIOStreams() {
        FileScanner fileScanner = new FileScanner();
        return Helper.EXECUTION_OF_INPUT_STREAM + fileScanner.readFileWithFileInputSteam() +
                Helper.EXECUTION_OF_BUF_INPUT_STREAM + fileScanner.readFileWithBufferedInputStream(Helper.BUFFER_SIZE);
    }

    public String filterComments() {
        return new CommentScanner().getCommentsFromFile(properties.getProperty("javaFile"));
    }

    public String readWithCustomStream() {
        return new FileScanner().readWithCustomScanner();
    }

    public String processFileManager(String command) {
        return fileManager.process(command);
    }

    public String readWithSomeBuffer() {
        return someBuffer.readBytes(file, Helper.SOME_BUFFER_CAPACITY);
    }

    public String writeWithSomeBuffer() {
         someBuffer.writeBytes(file.toPath(), Helper.TEXT_FOR_SOME_BUFFER);
         return someBuffer.readBytes(file, Helper.SOME_BUFFER_CAPACITY);
    }
}
