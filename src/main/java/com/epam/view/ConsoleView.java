package com.epam.view;

import com.epam.controller.Controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements View {
    private static Logger logger = LogManager.getLogger(ConsoleView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public ConsoleView() {
        controller = new Controller();
        generateMenu();
        putMethods();
    }

    private void outputMenu() {
        logger.info("\nMENU\n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
        logger.info("Enter Q to exit");
    }

    public void show() {
        Scanner in = new Scanner(System.in);
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = in.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Serialize and deserialize Ship and Droids");
        menu.put("2", "2 - Compare reading and writing of buffered and usual reader");
        menu.put("3", "3 - Display comments from java source file");
        menu.put("4", "4 - Show work of input stream with push method");
        menu.put("5", "5 - Display work of file manager");
        menu.put("6", "6 - Display work of buffer");
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showSerializedObjects);
        methodsMenu.put("2", this::compareBufferedAndUsualReader);
        methodsMenu.put("3", this::displayComments);
        methodsMenu.put("4", this::showPushMehtod);
        methodsMenu.put("5", this::showFileManager);
        methodsMenu.put("6", this::displaySomeBufferWork);
    }

    private void displaySomeBufferWork() {
        logger.info("Contents of file before writing:"+controller.readWithSomeBuffer());
        logger.info("Content after writing"+controller.writeWithSomeBuffer());
    }

    private void showFileManager() {
        logger.info("Enter:" +
                "cd - to change current directory\n" +
                "dir - to display list of files and folders in directory\n" +
                "q - to go back to menu\n");

        Scanner in = new Scanner(System.in);
//        FileManager fileManager = new FileManager();
        String com = in.nextLine();
        while (!in.nextLine().equals("q")) {
            logger.info("result: " + controller.processFileManager(com));
            com = in.nextLine();
        }
    }

    private void showPushMehtod() {
        logger.info(controller.readWithCustomStream());
    }

    private void displayComments() {
        logger.info(controller.filterComments());
    }

    private void compareBufferedAndUsualReader() {
        logger.info(controller.compareIOStreams());
    }

    private void showSerializedObjects() {
        try {
            logger.info("Serialized ship with droids: " + controller.testSerialization());
            logger.info("Deserialized ship with droids: " + controller.testDeserialization());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
