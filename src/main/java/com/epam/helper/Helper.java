package com.epam.helper;

import com.epam.models.shipanddroids.Droid;

public class Helper {
    public static final String CUSTOM_SCANNER_READ = "Read data: ";
    public static final String CUSTOM_SCANNER_AFTER_UNREAD = "Read data after calling unread method: ";
    public static final Droid FIRST_DROID = new Droid("Droid1", 100, 2008);
    public static final Droid SECOND_DROID = new Droid("Droid2", 50, 2018);
    public static final String SHIP_NAME = "Ship1";
    public static final int SHIP_TONNAGE = 100;
    public static final int SOME_BUFFER_CAPACITY = 4;
    public static final String EXECUTION_OF_INPUT_STREAM = "Execution time of FileInputStream: ";
    public static final String EXECUTION_OF_BUF_INPUT_STREAM = "\nExecution time of BufferedInputStream:";
    public static final int  BUFFER_SIZE = 100;
    public static final String PROPERTIES_FILE_NAME = "config.properties";
    public static final String TEXT_FOR_SOME_BUFFER = "KSEH IUHWEN IUWE";

}
